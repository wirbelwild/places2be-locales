# Changes in Locales

## 1.7.0 2019-11-25

### Changed 

*   Renamed all exceptions for better readybility. 

### Added 

*   Added the possibility to return a country code from a language code by calling `$myLanguageCode->getCountryCode()`. If the language code was set unspecific `NoCountryInformationAvailableException` will be thrown.