# Changes in Locales

## 1.9.0 2020-06-09

### Added 

*   Added `Locales::countryCodeExists` and `Locales::languageCodeExists` to proof if a code exists.