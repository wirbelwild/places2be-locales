# Changes in Locales

## 1.8.0 2020-03-13

### Changed 

*   PHP >= 7.2 is now required.
*   Dependencies have been updated.