# Changes in Bit&Black Locales 1.12

## 1.12.1 2021-09-20

### Fixed 

- Fixed missing proof for existing char that could lead to warnings.

## 1.12.0 2021-07-05

### Added 

- The `LanguageCode` class has now a method `getLanguageCodeShort` that can return a language code without the country code part. 