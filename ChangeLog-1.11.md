# Changes in Bit&Black Locales 1.11

## 1.11.0 2021-01-07

### Changed 

-   The constants `LOCALES_IGNORE_COUNTRY_EXISTENCE`, `LOCALES_IGNORE_LANGUAGE_EXISTENCE` and `LOCALES_ALLOW_COUNTRY_UNSPECIFIC_LANGUAGE_CODES` have been deprecated and replaced by static setters. 