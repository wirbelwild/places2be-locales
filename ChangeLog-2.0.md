# Changes in Bit&Black Locales 2.0

## 2.0.0 2023-05-30

### Changed

-   PHP >=8.2 is now required.
-   Deprecated methods have been removed.
-   The classes `CountryCode` and `LanguageCode` implement the `JsonSerializable` interface now.