<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Tests;

use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\CountryCode;
use Places2Be\Locales\CountryCodesEnum;
use Places2Be\Locales\Exception\CountryCodeNotFoundException;
use Places2Be\Locales\Exception\InvalidCountryCodeException;

/**
 * Tests in class CountryCode
 */
class CountryCodeTest extends TestCase
{
    /**
     * A not existing but well-formed country code
     */
    private string $countryCodeNotExisting = 'xy';

    /**
     * A wrong formed country code
     */
    private string $countryCodeWrong = 'xyz';

    /**
     * Tests if an exception is thrown when the country code is invalid
     *
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     */
    public function testThrowsExceptionOnWrongCountryCode(): void
    {
        $this->expectException(InvalidCountryCodeException::class);
        new CountryCode($this->countryCodeWrong);
    }

    /**
     * Tests if an exception is thrown when the country code doesn't exist
     *
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     */
    public function testThrowsExceptionOnNotExistingCountryCode(): void
    {
        $this->expectException(CountryCodeNotFoundException::class);
        new CountryCode($this->countryCodeNotExisting);
    }

    /**
     * Tests if the use of the constant prevents throwing an exception
     *
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     */
    #[RunInSeparateProcess]
    #[PreserveGlobalState(false)]
    public function testCanIgnoreExistenceCheck(): void
    {
        CountryCode::ignoreCountryExistence();
        
        $countryCode = new CountryCode($this->countryCodeNotExisting);
        
        self::assertSame(
            $countryCode->getCountryCode(),
            $this->countryCodeNotExisting
        );
    }

    public function testCanSerialize(): void
    {
        $languageCode = new CountryCode('de');
        $serialize = $languageCode->jsonSerialize();

        self::assertSame(
            'de',
            $serialize
        );
    }

    public function testCanInitializeWithEnum(): void
    {
        $countryCode = new CountryCode(
            CountryCodesEnum::DE
        );

        self::assertSame(
            'de',
            $countryCode->getCountryCode()
        );
    }
}
