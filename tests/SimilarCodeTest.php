<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Tests;

use PHPUnit\Framework\TestCase;
use Places2Be\Locales\SimilarCode;

/**
 * Class SimilarCodeTest
 *
 * @package Places2Be\Locales\Tests
 */
class SimilarCodeTest extends TestCase
{
    public function testCanFindSimilarCode(): void
    {
        $similarCode = new SimilarCode(
            'de-dr',
            [
                'bg-bg' => 'Bulgarian',
                'de-de' => 'German',
            ]
        );
        
        self::assertIsArray(
            $similarCode->getSimilarCodes()
        );
        
        self::assertArrayHasKey(
            0,
            $similarCode->getSimilarCodes()
        );
        
        self::assertSame(
            'de-de',
            $similarCode->getSimilarCodes()[0]
        );
    }
}
