<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Tests;

use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\Exception\CountryCodeNotFoundException;
use Places2Be\Locales\Exception\InvalidCountryCodeException;
use Places2Be\Locales\Exception\InvalidLanguageCodeException;
use Places2Be\Locales\Exception\LanguageCodeNotFoundException;
use Places2Be\Locales\Exception\NoCountryInformationAvailableException;
use Places2Be\Locales\LanguageCode;
use Places2Be\Locales\LanguageCodesEnum;

/**
 *  Tests in class LanguageCode
 */
class LanguageCodeTest extends TestCase
{
    /**
     * A not existing but well-formed language code
     */
    private string $languageCodeNotExisting = 'de-xy';

    /**
     * A wrong formed language code
     */
    private string $languageCodeWrong = 'xyz';

    /**
     * A short language code
     */
    private string $languageCodeShort = 'de';

    /**
     * Tests if an exception is thrown when the language code is invalid
     *
     * @throws InvalidLanguageCodeException
     * @throws LanguageCodeNotFoundException
     */
    public function testThrowsExceptionOnWrongLanguageCode(): void
    {
        $this->expectException(InvalidLanguageCodeException::class);
        new LanguageCode($this->languageCodeWrong);
    }

    /**
     * Tests if an exception is thrown when the language code doesn't exist
     *
     * @throws InvalidLanguageCodeException
     * @throws LanguageCodeNotFoundException
     */
    public function testThrowsExceptionOnNotExistingLanguageCode(): void
    {
        $this->expectException(LanguageCodeNotFoundException::class);
        new LanguageCode($this->languageCodeNotExisting);
    }

    /**
     * Tests if the use of the constant prevents throwing an exception
     *
     * @throws InvalidLanguageCodeException
     * @throws LanguageCodeNotFoundException
     */
    #[RunInSeparateProcess]
    #[PreserveGlobalState(false)]
    public function testCanIgnoreExistenceCheck(): void
    {
        LanguageCode::ignoreLanguageExistence();
        
        $languageCode = new LanguageCode($this->languageCodeNotExisting);
        
        self::assertSame(
            $languageCode->getLanguageCode(),
            $this->languageCodeNotExisting
        );
    }

    /**
     * Tests if the use of the constant allows short code languages
     *
     * @throws InvalidLanguageCodeException
     * @throws LanguageCodeNotFoundException
     */
    #[RunInSeparateProcess]
    #[PreserveGlobalState(false)]
    public function testCanValidateShortCodes(): void
    {
        LanguageCode::allowCountryUnspecificLanguageCode();
        
        $languageCode = new LanguageCode($this->languageCodeShort);
        
        self::assertSame(
            $languageCode->getLanguageCode(),
            $this->languageCodeShort
        );
    }

    /**
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     * @throws NoCountryInformationAvailableException
     */
    public function testCanReturnCountryCode(): void
    {
        $languageCode = new LanguageCode('de-at');
        $countryCode = $languageCode->getCountryCode();

        self::assertSame(
            'at',
            $countryCode->getCountryCode()
        );
    }

    /**
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     * @throws NoCountryInformationAvailableException
     */
    #[RunInSeparateProcess]
    #[PreserveGlobalState(false)]
    public function testThrowsNoCountryInformationAvailableException(): void
    {
        $this->expectException(NoCountryInformationAvailableException::class);
        
        LanguageCode::allowCountryUnspecificLanguageCode();
        
        $languageCode = new LanguageCode('de');
        $countryCode = $languageCode->getCountryCode();
        
        unset($countryCode);
    }

    #[RunInSeparateProcess]
    #[PreserveGlobalState(false)]
    public function testCanGetShortCode(): void
    {
        $languageCode = new LanguageCode('de-ch');

        self::assertSame(
            'de',
            $languageCode->getLanguageCodeShort()
        );
        
        LanguageCode::allowCountryUnspecificLanguageCode();
        
        $languageCode = new LanguageCode('en');

        self::assertSame(
            'en',
            $languageCode->getLanguageCodeShort()
        );
    }

    public function testCanSerialize(): void
    {
        $languageCode = new LanguageCode('de-ch');
        $serialize = $languageCode->jsonSerialize();

        self::assertSame(
            [
                'languageCode' => 'de-ch',
                'languageCodeShort' => 'de',
            ],
            $serialize
        );
    }

    public function testCanInitializeWithEnum(): void
    {
        $languageCode = new LanguageCode(
            LanguageCodesEnum::DE_CH
        );

        self::assertSame(
            'de-ch',
            $languageCode->getLanguageCode()
        );

        self::assertSame(
            'de',
            $languageCode->getLanguageCodeShort()
        );
    }
}
