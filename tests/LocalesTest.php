<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Tests;

use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\Locales;

/**
 * Class LocalesTest
 *
 * @package Places2Be\Locales\Tests
 */
class LocalesTest extends TestCase
{
    public static function getCanHandleCountryCodesData(): Generator
    {
        yield [
            'de',
            true,
        ];

        yield [
            'DE',
            true,
        ];

        yield [
            'de-ch',
            false,
        ];
    }

    #[DataProvider('getCanHandleCountryCodesData')]
    public function testCanHandleCountryCodes(string $countryCode, bool $existsExpected): void
    {
        self::assertSame(
            $existsExpected,
            Locales::countryCodeExists($countryCode),
        );
    }

    public static function getCanHandleLanguageCodesData(): Generator
    {
        yield [
            'de-de',
            true,
        ];

        yield [
            'DE-DE',
            true,
        ];

        yield [
            'de',
            false,
        ];
    }

    #[DataProvider('getCanHandleLanguageCodesData')]
    public function testCanHandleLanguageCodes(string $languageCode, bool $existsExpected): void
    {
        self::assertSame(
            $existsExpected,
            Locales::languageCodeExists($languageCode),
        );
    }
}
