<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Tests;

use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\LanguageCode;
use Places2Be\Locales\ReadingMode;
use Places2Be\Locales\ReadingModeEnum;

class ReadingModeTest extends TestCase
{
    public static function getGetReadingModeData(): Generator
    {
        yield [
            new LanguageCode('de-at'),
            ReadingModeEnum::LEFT_TO_RIGHT,
        ];

        yield [
            new LanguageCode('de-de'),
            ReadingModeEnum::LEFT_TO_RIGHT,
        ];

        yield [
            new LanguageCode('ar-jo'),
            ReadingModeEnum::RIGHT_TO_LEFT,
        ];

        yield [
            new LanguageCode('vi-vn'),
            ReadingModeEnum::TOP_TO_BOTTOM,
        ];
    }

    /**
     * @param LanguageCode $code
     * @param ReadingModeEnum $readingModeEnumExpected
     */
    #[DataProvider('getGetReadingModeData')]
    public function testGetReadingMode(LanguageCode $code, ReadingModeEnum $readingModeEnumExpected): void
    {
        self::assertSame(
            $readingModeEnumExpected,
            ReadingMode::getReadingMode($code)
        );
    }

    public function testCanGetReadingModeInLanguageCode(): void
    {
        $languageCode = new LanguageCode('de-de');

        self::assertSame(
            ReadingModeEnum::LEFT_TO_RIGHT,
            $languageCode->getReadingMode()
        );
    }
}
