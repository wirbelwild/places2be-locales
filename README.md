[![PHP from Packagist](https://img.shields.io/packagist/php-v/places2be/locales)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/places2be/locales/v/stable)](https://packagist.org/packages/places2be/locales)
[![Total Downloads](https://poser.pugx.org/places2be/locales/downloads)](https://packagist.org/packages/places2be/locales)
[![License](https://poser.pugx.org/places2be/locales/license)](https://packagist.org/packages/places2be/locales)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Places2Be Locales

Handling country codes and language codes in an object-oriented way.

## TOC

-   [Installation](#installation)
-   [Usage](#usage)
    -   [Handling country codes](#handling-country-codes)
    -   [Handling language codes](#handling-language-codes)
    -   [Handling the reading mode](#handling-the-reading-mode)
-   [Help](#help)

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/places2be/locales). Add it to your project by running `$ composer require places2be/locales`. 

## Usage

### Handling country codes

Set up a country code from a string like that:

```php
<?php

use Places2Be\Locales\CountryCode;

$countryCode = new CountryCode('de');
```

The script will only accept country codes having a length of two characters and will throw an `InvalidCountryCode` exception otherwise.

Per default, the script will proof if the country code exists. Ignore the check by writing:

```php
<?php

use Places2Be\Locales\CountryCode;

CountryCode::ignoreCountryExistence();

$countryCode = new CountryCode('xy');
```

Alternatively, you can initialise the class with a [CountryCodesEnum](./src/CountryCodesEnum.php):

```php
<?php

use Places2Be\Locales\CountryCode;
use Places2Be\Locales\CountryCodesEnum;

$countryCode = new CountryCode(
    CountryCodesEnum::DE
);
```

### Handling language codes

Set up a language code from a string:

```php
<?php

use Places2Be\Locales\LanguageCode;

$languageCode = new LanguageCode('de-ch');
```

The script will only accept language codes written like that: `xx-xx` (or `xxx-xx`) and will throw an `InvalidLanguageCode` exception otherwise.

Per default, the script will proof if the language code exists. Ignore the check by writing:

```php
<?php

use Places2Be\Locales\LanguageCode;

LanguageCode::ignoreLanguageExistence();

$languageCode = new LanguageCode('de-xx');
```

If you want to use country-unspecific language codes like `de` instead of `de-de` you can allow that by writing `LanguageCode::allowCountryUnspecificLanguageCode()`.

Alternatively, you can initialise the class with a [LanguageCodesEnum](./src/LanguageCodesEnum.php):

```php
<?php

use Places2Be\Locales\LanguageCode;
use Places2Be\Locales\LanguageCodesEnum;

$languageCode = new LanguageCode(
    LanguageCodesEnum::DE_DE
);
```

### Handling the reading mode

You can also access the reading mode from a language code by calling the `getReadingMode` method. It will return a instance of the [ReadingModeEnum](./src/ReadingModeEnum.php).

```php
<?php

use Places2Be\Locales\LanguageCode;

$languageCode = new LanguageCode('ar-ae');
$readingMode = $languageCode->getReadingMode();
```

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).