# Changes in Locales

## 1.10.0 2020-11-18

### Fixed

*   Replaced slashes by `DIRECTORY_SEPARATOR` to add support for Windows.

### Changed 

*   Changed composer.json to allow PHP ^8.0.