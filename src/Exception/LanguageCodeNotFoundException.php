<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Exception;

use Places2Be\Locales\Exception;
use Places2Be\Locales\SimilarCode;

/**
 *  Handles not existing language codes
 */
class LanguageCodeNotFoundException extends Exception
{
    /**
     * LanguageCodeNotFoundException constructor.
     *
     * @param array<string, string> $countryCodes
     */
    public function __construct(string $languageCode, array $countryCodes, SimilarCode $countryCodeSimilar)
    {
        $textAdditional = '';
        
        if ('' !== (string) $countryCodeSimilar) {
            $textAdditional = ' Do you mean "' . $countryCodeSimilar . '" for ' . $countryCodes[(string) $countryCodeSimilar] . '?';
        }
        
        parent::__construct('Couldn\'t find language code "' . $languageCode . '".' . $textAdditional);
    }
}
