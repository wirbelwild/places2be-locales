<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Exception;

use Places2Be\Locales\Exception;

/**
 * Class NoCountryInformationAvailableException
 *
 * @package Places2Be\Locales\Exception
 */
class NoCountryInformationAvailableException extends Exception
{
    /**
     * NoCountryInformationAvailableException constructor.
     */
    public function __construct(string $languageCode)
    {
        parent::__construct(
            'Language code "' . $languageCode . '" is unspecific and can\'t give a proof country information. ' .
            'Use language codes like "de-ch" instead.'
        );
    }
}
