<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Exception;

use Places2Be\Locales\Exception;

/**
 *  Handles invalid country codes
 */
class InvalidCountryCodeException extends Exception
{
    /**
     * InvalidCountryCodeException constructor.
     */
    public function __construct(string $countryCode)
    {
        parent::__construct('Country code should be like "de" but is "' . $countryCode . '" instead');
    }
}
