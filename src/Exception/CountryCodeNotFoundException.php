<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Exception;

use Places2Be\Locales\Exception;
use Places2Be\Locales\SimilarCode;

/**
 *  Handles not existing country codes
 */
class CountryCodeNotFoundException extends Exception
{
    /**
     * CountryCodeNotFoundException constructor.
     *
     * @param array<string, string> $countryCodes
     */
    public function __construct(string $countryCode, array $countryCodes, SimilarCode $countryCodeSimilar)
    {
        $textAdditional = '';

        if ('' !== (string) $countryCodeSimilar) {
            $textAdditional = ' Do you mean "' . $countryCodeSimilar . '" for ' . $countryCodes[(string) $countryCodeSimilar] . '?';
        }

        parent::__construct('Couldn\'t find country code "' . $countryCode . '".' . $textAdditional);
    }
}
