<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales\Exception;

use Places2Be\Locales\Exception;

/**
 *  Handles invalid language codes
 */
class InvalidLanguageCodeException extends Exception
{
    /**
     * InvalidLanguageCodeException constructor.
     */
    public function __construct(string $languageCode)
    {
        parent::__construct('Language code should be like "de-ch" but is "' . $languageCode . '" instead');
    }
}
