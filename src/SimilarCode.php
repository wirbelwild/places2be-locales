<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

use Stringable;

readonly class SimilarCode implements Stringable
{
    /**
     * @var array<int, string>
     */
    private array $similarCodes;
    
    /**
     * @param array<string, string> $languageCodesLong
     */
    public function __construct(string $languageCode, array $languageCodesLong)
    {
        $percentsAll = [];

        foreach (array_keys($languageCodesLong) as $key) {
            $percent = $this->getSimilarity($key, $languageCode) * 100;
            $percentsAll[$key] = round($percent);
        }

        arsort($percentsAll);
        $this->similarCodes = array_keys($percentsAll);
    }

    /**
     * @return array<int, string>
     */
    public function getSimilarCodes(): array
    {
        return $this->similarCodes;
    }

    public function __toString(): string
    {
        return $this->getSimilarCodes()[0] ?? '';
    }

    /**
     * Thanks to vasyl@vasyltech.com
     */
    private function getSimilarity(string $string1, string $string2): float
    {
        $string1Length = strlen($string1);
        $string2Length = strlen($string2);

        $max = max($string1Length, $string2Length);
        $similarity = $counter1 = $counter2 = 0;

        while (isset($string1[$counter1], $string2[$counter2]) && ($counter1 < $string1Length)) {
            if ($string1[$counter1] === $string2[$counter2]) {
                ++$similarity;
                ++$counter1;
                ++$counter2;
            } elseif ($string1Length < $string2Length) {
                ++$string1Length;
                ++$counter2;
            } elseif ($string1Length > $string2Length) {
                ++$counter1;
                --$string1Length;
            } else {
                ++$counter1;
                ++$counter2;
            }
        }

        return $similarity / $max;
    }
}
