<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

/**
 * Class Locales
 *
 * @package Places2Be\Locales
 */
class Locales
{
    /**
     * Returns if a country code exists.
     */
    public static function countryCodeExists(string $countryCode): bool
    {
        try {
            new CountryCode($countryCode);
        } catch (Exception) {
            return false;
        }

        return true;
    }
    
    /**
     * Returns if a language code exists.
     */
    public static function languageCodeExists(string $languageCode): bool
    {
        try {
            new LanguageCode($languageCode);
        } catch (Exception) {
            return false;
        }

        return true;
    }
}
