<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

class ReadingMode
{
    /**
     * @todo Change this to an Enum in v4.
     * @var array<int, string>
     */
    public static array $countryCodesLeftToRight = [
        'aa', 'ab', 'af', 'ak', 'sq', 'am', 'an', 'hy', 'as', 'av', 'ae',
        'ay', 'az', 'ba', 'bm', 'eu', 'be', 'bn', 'bh', 'bi', 'bs', 'br',
        'bg', 'my', 'ca', 'ch', 'ce', 'cu', 'cv', 'kw', 'co', 'cr', 'cs',
        'da', 'nl', 'dz', 'en', 'eo', 'et', 'ee', 'fo', 'fj', 'fi', 'fr',
        'fy', 'ff', 'ka', 'de', 'gd', 'ga', 'gl', 'gv', 'el', 'gn', 'gu',
        'ht', 'ha', 'hz', 'hi', 'ho', 'hr', 'hu', 'ig', 'is', 'io', 'ii',
        'iu', 'ie', 'ia', 'id', 'ik', 'it', 'jv', 'kl', 'kn', 'kr', 'kk',
        'km', 'ki', 'rw', 'ky', 'kv', 'kg', 'kj', 'lo', 'la', 'lv', 'li',
        'ln', 'lt', 'lb', 'lu', 'lg', 'mk', 'mh', 'ml', 'mi', 'mr', 'ms',
        'mg', 'mt', 'na', 'nv', 'nr', 'nd', 'ng', 'ne', 'nn', 'nb', 'no',
        'ny', 'oc', 'oj', 'or', 'om', 'os', 'pl', 'pt', 'qu', 'rm', 'ro',
        'rn', 'ru', 'sg', 'sa', 'si', 'sk', 'sl', 'se', 'sm', 'sn', 'so',
        'st', 'es', 'sc', 'sr', 'ss', 'su', 'sw', 'sv', 'ty', 'ta', 'tt',
        'te', 'tg', 'tl', 'th', 'bo', 'ti', 'to', 'tn', 'ts', 'tr', 'tw',
        'uk', 'uz', 've', 'vo', 'cy', 'wa', 'wo', 'xh', 'yo', 'zu',
    ];

    /**
     * @todo Change this to an Enum in v4.
     * @var array<int, string>
     */
    public static array $countryCodesRightToLeft = [
        'ar', 'dv', 'he', 'ks', 'ku', 'fa', 'ps', 'sd', 'ug', 'ur', 'yi',
    ];

    /**
     * @todo Change this to an Enum in v4.
     * @var array<int, string>
     */
    public static array $countryCodesTopToBottom = [
        'zh', 'ja', 'ko', 'mn', 'vi', 'za',
    ];

    /**
     * @param LanguageCode $code
     * @return ReadingModeEnum
     */
    public static function getReadingMode(LanguageCode $code): ReadingModeEnum
    {
        $languageCodeShort = $code->getLanguageCodeShort();

        if (in_array($languageCodeShort, self::$countryCodesLeftToRight, false)) {
            return ReadingModeEnum::LEFT_TO_RIGHT;
        }

        if (in_array($languageCodeShort, self::$countryCodesRightToLeft, false)) {
            return ReadingModeEnum::RIGHT_TO_LEFT;
        }

        if (in_array($languageCodeShort, self::$countryCodesTopToBottom, false)) {
            return ReadingModeEnum::TOP_TO_BOTTOM;
        }

        return ReadingModeEnum::UNKNOWN;
    }
}
