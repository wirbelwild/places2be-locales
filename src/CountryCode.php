<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

use JsonSerializable;
use Places2Be\Locales\Exception\CountryCodeNotFoundException;
use Places2Be\Locales\Exception\InvalidCountryCodeException;
use Stringable;

/**
 *  Handles a country code.
 */
class CountryCode implements Stringable, JsonSerializable
{
    /**
     * @todo Change this to an Enum in v4.
     */
    private readonly string $countryCode;

    private static bool $ignoreCountryExistence = false;

    /**
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     */
    public function __construct(string|CountryCodesEnum $countryCode)
    {
        if ($countryCode instanceof CountryCodesEnum) {
            $countryCode = $countryCode->name;
            $countryCode = strtolower($countryCode);
            $this->countryCode = $countryCode;
            return;
        }

        $countryCode = mb_strtolower($countryCode);

        if (!preg_match('/^[a-z]{2}$/', $countryCode)) {
            throw new InvalidCountryCodeException($countryCode);
        }
        
        if (true !== self::$ignoreCountryExistence) {
            $countries = CountryCodesEnum::getAllCountryCodes();
            $allCodes = [];

            foreach ($countries as $countryCodeRaw => $countryName) {
                $countryCodeRaw = str_replace('_', '-', $countryCodeRaw);
                $countryCodeRaw = mb_strtolower($countryCodeRaw);
                $allCodes[$countryCodeRaw] = $countryName;
            }

            if (!array_key_exists($countryCode, $allCodes)) {
                $similar = new SimilarCode($countryCode, $allCodes);
                throw new CountryCodeNotFoundException($countryCode, $allCodes, $similar);
            }
        }

        $this->countryCode = $countryCode;
    }

    /**
     *  Gets country code
     */
    public function __toString(): string
    {
        return $this->getCountryCode();
    }

    public function jsonSerialize(): string
    {
        return $this->getCountryCode();
    }

    /**
     *  Gets country code
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public static function isCountryExistenceIgnored(): bool
    {
        return self::$ignoreCountryExistence;
    }
    
    /**
     * Ignores the country existence, so a non-existing country code can be used.
     */
    public static function ignoreCountryExistence(): void
    {
        self::$ignoreCountryExistence = true;
    }

    /**
     * Requires the country existence, so a non-existing country code can't be handled.
     */
    public static function requireCountryExistence(): void
    {
        self::$ignoreCountryExistence = false;
    }
}
