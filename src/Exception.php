<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

/**
 *  Handles errors
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
