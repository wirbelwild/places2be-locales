<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

enum LanguageCodesEnum: string
{
    case AF_ZA = 'Afrikaans (South Africa)';
    case AR_AE = 'Arabic (U.A.E.)';
    case AR_BH = 'Arabic (Bahrain)';
    case AR_DZ = 'Arabic (Algeria)';
    case AR_EG = 'Arabic (Egypt)';
    case AR_IQ = 'Arabic (Iraq)';
    case AR_JO = 'Arabic (Jordan)';
    case AR_KW = 'Arabic (Kuwait)';
    case AR_LB = 'Arabic (Lebanon)';
    case AR_LY = 'Arabic (Libya)';
    case AR_MA = 'Arabic (Morocco)';
    case AR_OM = 'Arabic (Oman)';
    case AR_QA = 'Arabic (Qatar)';
    case AR_SA = 'Arabic (Saudi Arabia)';
    case AR_SY = 'Arabic (Syria)';
    case AR_TN = 'Arabic (Tunisia)';
    case AR_YE = 'Arabic (Yemen)';
    case AZ_AZ = 'Azeri (Azerbaijan)';
    case BE_BY = 'Belarusian (Belarus)';
    case BG_BG = 'Bulgarian (Bulgaria)';
    case BS_BA = 'Bosnian (Bosnia and Herzegovina)';
    case CA_ES = 'Catalan (Spain)';
    case CS_CZ = 'Czech (Czech Republic)';
    case CY_GB = 'Welsh (United Kingdom)';
    case DA_DK = 'Danish (Denmark)';
    case DE_AT = 'German (Austria)';
    case DE_CH = 'German (Switzerland)';
    case DE_DE = 'German (Germany)';
    case DE_LI = 'German (Liechtenstein)';
    case DE_LU = 'German (Luxembourg)';
    case DV_MV = 'Divehi (Maldives)';
    case EL_GR = 'Greek (Greece)';
    case EN_AU = 'English (Australia)';
    case EN_BZ = 'English (Belize)';
    case EN_CA = 'English (Canada)';
    case EN_CB = 'English (Caribbean)';
    case EN_GB = 'English (United Kingdom)';
    case EN_IE = 'English (Ireland)';
    case EN_JM = 'English (Jamaica)';
    case EN_NZ = 'English (New Zealand)';
    case EN_PH = 'English (Republic of the Philippines)';
    case EN_TT = 'English (Trinidad and Tobago)';
    case EN_US = 'English (United States)';
    case EN_ZA = 'English (South Africa)';
    case EN_ZW = 'English (Zimbabwe)';
    case ES_AR = 'Spanish (Argentina)';
    case ES_BO = 'Spanish (Bolivia)';
    case ES_CL = 'Spanish (Chile)';
    case ES_CO = 'Spanish (Colombia)';
    case ES_CR = 'Spanish (Costa Rica)';
    case ES_DO = 'Spanish (Dominican Republic)';
    case ES_EC = 'Spanish (Ecuador)';
    case ES_ES = 'Spanish (Spain)';
    case ES_GT = 'Spanish (Guatemala)';
    case ES_HN = 'Spanish (Honduras)';
    case ES_MX = 'Spanish (Mexico)';
    case ES_NI = 'Spanish (Nicaragua)';
    case ES_PA = 'Spanish (Panama)';
    case ES_PE = 'Spanish (Peru)';
    case ES_PR = 'Spanish (Puerto Rico)';
    case ES_PY = 'Spanish (Paraguay)';
    case ES_SV = 'Spanish (El Salvador)';
    case ES_UY = 'Spanish (Uruguay)';
    case ES_VE = 'Spanish (Venezuela)';
    case ET_EE = 'Estonian (Estonia)';
    case EU_ES = 'Basque (Spain)';
    case FA_IR = 'Farsi (Iran)';
    case FI_FI = 'Finnish (Finland)';
    case FO_FO = 'Faroese (Faroe Islands)';
    case FR_BE = 'French (Belgium)';
    case FR_CA = 'French (Canada)';
    case FR_CH = 'French (Switzerland)';
    case FR_FR = 'French (France)';
    case FR_LU = 'French (Luxembourg)';
    case FR_MC = 'French (Principality of Monaco)';
    case GL_ES = 'Galician (Spain)';
    case GU_IN = 'Gujarati (India)';
    case HE_IL = 'Hebrew (Israel)';
    case HI_IN = 'Hindi (India)';
    case HR_BA = 'Croatian (Bosnia and Herzegovina)';
    case HR_HR = 'Croatian (Croatia)';
    case HU_HU = 'Hungarian (Hungary)';
    case HY_AM = 'Armenian (Armenia)';
    case ID_ID = 'Indonesian (Indonesia)';
    case IS_IS = 'Icelandic (Iceland)';
    case IT_CH = 'Italian (Switzerland)';
    case IT_IT = 'Italian (Italy)';
    case JA_JP = 'Japanese (Japan)';
    case KA_GE = 'Georgian (Georgia)';
    case KK_KZ = 'Kazakh (Kazakhstan)';
    case KN_IN = 'Kannada (India)';
    case KO_KR = 'Korean (Korea)';
    case KOK_IN = 'Konkani (India)';
    case KY_KG = 'Kyrgyz (Kyrgyzstan)';
    case LT_LT = 'Lithuanian (Lithuania)';
    case LV_LV = 'Latvian (Latvia)';
    case MI_NZ = 'Maori (New Zealand)';
    case MK_MK = 'FYRO Macedonian (Former Yugoslav Republic of Macedonia)';
    case MN_MN = 'Mongolian (Mongolia)';
    case MR_IN = 'Marathi (India)';
    case MS_BN = 'Malay (Brunei Darussalam)';
    case MS_MY = 'Malay (Malaysia)';
    case MT_MT = 'Maltese (Malta)';
    case NB = 'Norwegian (Bokmål)';
    case NB_NO = 'Norwegian (Bokmål) (Norway)';
    case NL_BE = 'Dutch (Belgium)';
    case NL_NL = 'Dutch (Netherlands)';
    case NN_NO = 'Norwegian (Nynorsk) (Norway)';
    case NS_ZA = 'Northern Sotho (South Africa)';
    case PA_IN = 'Punjabi (India)';
    case PL_PL = 'Polish (Poland)';
    case PS_AR = 'Pashto (Afghanistan)';
    case PT_BR = 'Portuguese (Brazil)';
    case PT_PT = 'Portuguese (Portugal)';
    case QU_BO = 'Quechua (Bolivia)';
    case QU_EC = 'Quechua (Ecuador)';
    case QU_PE = 'Quechua (Peru)';
    case RO_RO = 'Romanian (Romania)';
    case RU_RU = 'Russian (Russia)';
    case SA_IN = 'Sanskrit (India)';
    case SE_FI = 'Sami (Finland)';
    case SE_NO = 'Sami (Norway)';
    case SE_SE = 'Sami (Sweden)';
    case SK_SK = 'Slovak (Slovakia)';
    case SL_SI = 'Slovenian (Slovenia)';
    case SQ_AL = 'Albanian (Albania)';
    case SR_BA = 'Serbian (Bosnia and Herzegovina)';
    case SR_SP = 'Serbian (Serbia and Montenegro)';
    case SV_FI = 'Swedish (Finland)';
    case SV_SE = 'Swedish (Sweden)';
    case SW_KE = 'Swahili (Kenya)';
    case SYR_SY = 'Syriac (Syria)';
    case TA_IN = 'Tamil (India)';
    case TE_IN = 'Telugu (India)';
    case TH_TH = 'Thai (Thailand)';
    case TL_PH = 'Tagalog (Philippines)';
    case TN_ZA = 'Tswana (South Africa)';
    case TR_TR = 'Turkish (Turkey)';
    case TT_RU = 'Tatar (Russia)';
    case UK_UA = 'Ukrainian (Ukraine)';
    case UR_PK = 'Urdu (Islamic Republic of Pakistan)';
    case UZ = 'Uzbek (Latin)';
    case UZ_UZ = 'Uzbek (Uzbekistan)';
    case VI = 'Vietnamese';
    case VI_VN = 'Vietnamese (Viet Nam)';
    case XH = 'Xhosa';
    case XH_ZA = 'Xhosa (South Africa)';
    case ZH_CN = 'Chinese (S)';
    case ZH_HK = 'Chinese (Hong Kong)';
    case ZH_MO = 'Chinese (Macau)';
    case ZH_SG = 'Chinese (Singapore)';
    case ZH_TW = 'Chinese (T)';
    case ZU_ZA = 'Zulu (South Africa)';

    /**
     * @return array<int, string>
     */
    public static function names(): array
    {
        return array_column(
            self::cases(),
            'name'
        );
    }

    /**
     * @return array<int, string>
     */
    public static function values(): array
    {
        return array_column(
            self::cases(),
            'value'
        );
    }

    /**
     * @return array<string, string>
     */
    public static function getAllLanguageCodes(): array
    {
        return array_combine(
            self::names(),
            self::values(),
        );
    }
}
