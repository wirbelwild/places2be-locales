<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

enum ReadingModeEnum: string
{
    case LEFT_TO_RIGHT = 'ltr';
    case RIGHT_TO_LEFT = 'rtl';
    case TOP_TO_BOTTOM = 'ttb';
    case UNKNOWN = 'unknown';
}
