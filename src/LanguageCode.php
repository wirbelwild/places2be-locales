<?php

/**
 * Places2Be Locales.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Locales;

use JsonSerializable;
use Places2Be\Locales\Exception\CountryCodeNotFoundException;
use Places2Be\Locales\Exception\InvalidCountryCodeException;
use Places2Be\Locales\Exception\InvalidLanguageCodeException;
use Places2Be\Locales\Exception\LanguageCodeNotFoundException;
use Places2Be\Locales\Exception\NoCountryInformationAvailableException;
use Stringable;

/**
 * The LanguageCode class handles a single language code.
 */
class LanguageCode implements Stringable, JsonSerializable
{
    /**
     * @todo Change this to an Enum in v4.
     */
    private readonly string $languageCode;

    private static bool $ignoreLanguageExistence = false;

    private static bool $allowCountryUnspecificLanguageCodes = false;
    
    private readonly string $languageCodeShort;

    /**
     * LanguageCode constructor.
     *
     * @throws InvalidLanguageCodeException
     * @throws LanguageCodeNotFoundException
     */
    public function __construct(string|LanguageCodesEnum $languageCode)
    {
        if ($languageCode instanceof LanguageCodesEnum) {
            $languageCode = $languageCode->name;
            $languageCode = strtolower($languageCode);
            $languageCode = str_replace('_', '-', $languageCode);

            $this->languageCode = $languageCode;

            $languageCodeParts = explode('-', $this->languageCode);
            $this->languageCodeShort = $languageCodeParts[0] ?? $this->languageCode;

            return;
        }

        $languageCode = mb_strtolower($languageCode);

        $locales = LanguageCodesEnum::getAllLanguageCodes();
        $languageCodesLong = [];

        foreach ($locales as $languageCodeRaw => $languageName) {
            $languageCodeRaw = str_replace('_', '-', $languageCodeRaw);
            $languageCodeRaw = mb_strtolower($languageCodeRaw);
            $languageCodesLong[$languageCodeRaw] = $languageName;
        }

        $languageCodesShort = array_map(
            static fn ($code) => explode('-', $code)[0],
            array_keys($languageCodesLong)
        );

        if (true !== self::$allowCountryUnspecificLanguageCodes && 0 === preg_match('/[a-z]{2,3}[-][a-z]{2}/', $languageCode)) {
            throw new InvalidLanguageCodeException($languageCode);
        }

        if (true === self::$allowCountryUnspecificLanguageCodes && 0 === preg_match('/[a-z]{2,3}/', $languageCode)) {
            throw new InvalidLanguageCodeException($languageCode);
        }

        if (true !== self::$ignoreLanguageExistence
            && !array_key_exists($languageCode, $languageCodesLong)
            && !in_array($languageCode, $languageCodesShort, true)
        ) {
            $similar = new SimilarCode($languageCode, $languageCodesLong);
            throw new LanguageCodeNotFoundException($languageCode, $languageCodesLong, $similar);
        }

        $this->languageCode = $languageCode;
        
        $languageCodeParts = explode('-', $this->languageCode);
        $this->languageCodeShort = $languageCodeParts[0] ?? $this->languageCode;
    }

    /**
     * Returns the language code in its longest available version.
     * If the country code has been set it will look like `en-gb`,
     * otherwise it will look like `en`.
     */
    public function __toString(): string
    {
        return $this->getLanguageCode();
    }

    /**
     * @return array{
     *     languageCode: string,
     *     languageCodeShort: string,
     * }
     */
    public function jsonSerialize(): array
    {
        return [
            'languageCode' => $this->getLanguageCode(),
            'languageCodeShort' => $this->getLanguageCodeShort(),
        ];
    }

    /**
     * Returns the language code in its longest available version.
     * If the country code has been set it will look like `en-gb`,
     * otherwise it will look like `en`.
     */
    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * Returns the language code in its short version.
     * For example if the language code is `de-ch`, the short version would be `de`,
     */
    public function getLanguageCodeShort(): string
    {
        return $this->languageCodeShort;
    }

    /**
     * Returns the country code from the current language code.
     * For example if the language code is `de-ch`, the country code would be `ch`,
     *
     * @throws CountryCodeNotFoundException
     * @throws InvalidCountryCodeException
     * @throws NoCountryInformationAvailableException
     */
    public function getCountryCode(): CountryCode
    {
        if (!str_contains($this->languageCode, '-')) {
            throw new NoCountryInformationAvailableException($this->languageCode);
        }

        $codes = explode('-', $this->languageCode);
        return new CountryCode($codes[1]);
    }

    public static function isLanguageExistenceIgnored(): bool
    {
        return self::$ignoreLanguageExistence;
    }

    /**
     * Ignores the language existence, so a non-existing language code can be used.
     */
    public static function ignoreLanguageExistence(): void
    {
        self::$ignoreLanguageExistence = true;
    }

    /**
     * Requires the language existence, so a non-existing language code can't be handled.
     */
    public static function requireLanguageExistence(): void
    {
        self::$ignoreLanguageExistence = false;
    }

    public static function isCountryUnspecificLanguageCodeAllowed(): bool
    {
        return self::$allowCountryUnspecificLanguageCodes;
    }

    /**
     * Allows country unspecific language codes, for example `de` instead of `de-de`.
     */
    public static function allowCountryUnspecificLanguageCode(): void
    {
        self::$allowCountryUnspecificLanguageCodes = true;
    }

    /**
     * Disallows country unspecific language codes. For example `de` is invalid then and `de-de` is valid.
     */
    public static function disallowCountryUnspecificLanguageCode(): void
    {
        self::$allowCountryUnspecificLanguageCodes = false;
    }


    /**
     * Returns the reading mode for the country, the current language code belongs to.
     *
     * @return ReadingModeEnum
     */
    public function getReadingMode(): ReadingModeEnum
    {
        return ReadingMode::getReadingMode($this);
    }
}
